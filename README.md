# CrOS eclass overlay

This eclass overlay is used for 2 purposes:

* [Eclass overrides for all overlays](https://www.chromium.org/chromium-os/developer-library/guides/portage/overlay-faq/#eclass-overlay).
* [Account management (user & group)](https://www.chromium.org/chromium-os/developer-library/reference/build/account-management/).
