# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

SLOT="0"
KEYWORDS="*"

inherit acct-group

ACCT_GROUP_ID=435
