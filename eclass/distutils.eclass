# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

die "Only ebuilds using distutils-r1 are allowed in CrOS now as distutils does not support Python 3 & cross-compiling"
